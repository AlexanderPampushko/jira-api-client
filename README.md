## Project overview
  Позволяет получить данные о лицензии Jira 7 приложений:
- Jira Core
- Jira Software
- Jira ServiceDesk
## Motivation
Необходимость управлять лицензиями Jira и получать статус лицензий из других приложений  
## Code Example
```
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pmpshk.jira.rest.api.client.entity.JiraLicense;
import pmpshk.jira.rest.api.client.service.JiraService;

import java.lang.invoke.MethodHandles;

/**
 *
 */
public class JiraClientTest
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public static void main(String[] args)
	{
		JiraService jiraService = new JiraService("https://your-jira.net", "admin", "password");
		JiraLicense jiraSoftwareLicense = jiraService.getJiraSoftwareLicense();
		JiraLicense jiraCoreLicense = jiraService.getJiraCoreLicense();
		JiraLicense jiraServiceDeskLicense = jiraService.getJiraServiceDeskLicense();
		log.info("jira core: {}", String.valueOf(jiraCoreLicense));
		log.info("jira service desk: {}", String.valueOf(jiraServiceDeskLicense));
		log.info("jira software: {}", String.valueOf(jiraSoftwareLicense));
	}
}
  
```
  
## Contributors
Alexander Pampushko
## License
  
Apache License 2.0

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pmpshk.jira.rest.api.client.entity.JiraLicense;
import pmpshk.jira.rest.api.client.service.JiraService;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 *
 */
public class JiraClientTest
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private static JiraService jiraService;
	
	@BeforeAll
	static void initAll() throws IOException
	{
		Properties settings = new Properties();
		//todo сделать нормально
		InputStream resource = JiraClientTest.class.getClassLoader().getResourceAsStream("test.properties");
		settings.load(resource);
		
		jiraService = new JiraService(settings.getProperty("baseUrl"), settings.getProperty("username"), settings
				.getProperty("password"));
	}
	
	@Test
	public void testGetJiraSoftwareLicense()
	{
		JiraLicense license = jiraService.getJiraSoftwareLicense();
		log.info("jira software: {}", String.valueOf(license));
		assertNotNull(license);
		assertAllLicenseProp("jira software license", license);
	}
	
	@Test
	public void testGetJiraCoreLicense()
	{
		JiraLicense license = jiraService.getJiraCoreLicense();
		log.info("jira core: {}", String.valueOf(license));
		assertNotNull(license);
		assertAllLicenseProp("jira core license", license);
	}
	
	@Test
	public void testGetJiraServiceDeskLicense()
	{
		JiraLicense license = jiraService.getJiraServiceDeskLicense();
		log.info("jira service desk: {}", String.valueOf(license));
		assertNotNull(license);
		assertAllLicenseProp("jira service desk license", license);
	}
	
	private void assertAllLicenseProp(String heading, JiraLicense license)
	{
		assertAll(heading,
				() -> assertNotNull(license.isDataCenter()),
				() -> assertNotNull(license.isEnterprise()),
				() -> assertNotNull(license.isActive()),
				() -> assertNotNull(license.isSubscription()),
				() -> assertNotNull(license.isValid()),
				() -> assertNotNull(license.isEvaluation()),
				() -> assertNotNull(license.getExpiryDate()),
				() -> assertNotNull(license.isExpired()),
				() -> assertNotNull(license.isAutoRenewal()),
				() -> assertNotNull(license.getMaximumNumberOfUsers())
		);
	}
}

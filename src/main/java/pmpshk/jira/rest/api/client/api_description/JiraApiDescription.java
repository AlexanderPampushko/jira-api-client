package pmpshk.jira.rest.api.client.api_description;

import pmpshk.jira.rest.api.client.entity.JiraLicense;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

//определяем интерфейс для ретрофита
public interface JiraApiDescription
{
	@Headers({"Content-Type: application/json"})
	@GET("rest/plugins/applications/1.0/installed/jira-software/license")
	Call<JiraLicense> getJiraSoftwareLicense();
	
	@Headers({"Content-Type: application/json"})
	@GET("rest/plugins/applications/1.0/installed/jira-servicedesk/license")
	Call<JiraLicense> getJiraServiceDeskLicense();
	
	@Headers({"Content-Type: application/json"})
	@GET("rest/plugins/applications/1.0/installed/jira-core/license")
	Call<JiraLicense> getJiraCoreLicense();
}

package pmpshk.jira.rest.api.client.retrofit;

import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class RetrofitHelper
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private static volatile RetrofitHelper retrofitHelper;
	
	public static RetrofitHelper getInstance()
	{
		if (retrofitHelper == null)
		{
			synchronized (RetrofitHelper.class)
			{
				if (retrofitHelper == null)
				{
					retrofitHelper = new RetrofitHelper();
				}
			}
		}
		
		return retrofitHelper;
	}
	
	private static OkHttpClient getOkHttpClient(String username, String password)
	{
		Interceptor customHeadersInterceptor = new Interceptor()
		{
			@Override
			public Response intercept(Chain chain) throws IOException
			{
				Request originalRequest = chain.request();
				Request request = originalRequest.newBuilder()
						
						.addHeader("User-Agent", "curl/7.47.0")
						.addHeader("X-Atlassian-Token", "nocheck")
						.addHeader("Content-Type", "application/json")
						.addHeader("Authorization", Credentials.basic(username, password))
						.build();
				
				return chain.proceed(request);
			}
		};
		
		//создаем интерсептор для логирования
		HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
		httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
		
		OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
		okHttpClient.connectTimeout(30, TimeUnit.SECONDS);
		okHttpClient.readTimeout(30, TimeUnit.SECONDS);
		okHttpClient.interceptors().add(httpLoggingInterceptor);
		okHttpClient.interceptors().add(customHeadersInterceptor);
		return okHttpClient.build();
	}
	
	public static <T> T getApiClient(String baseUrl, String username, String password, Class<T> clz)
	{
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(baseUrl)
				.addConverterFactory(GsonConverterFactory.create())
				.client(getOkHttpClient(username, password))
				.build();
		
		T t = retrofit.create(clz);
		return t;
	}
}

package pmpshk.jira.rest.api.client.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pmpshk.jira.rest.api.client.api_description.JiraApiDescription;
import pmpshk.jira.rest.api.client.entity.JiraLicense;
import pmpshk.jira.rest.api.client.retrofit.RetrofitHelper;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 *
 */
public class JiraService
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private JiraApiDescription jiraApiClient;
	
	public JiraService(String baseUrl, String username, String password)
	{
		RetrofitHelper retrofitHelper = RetrofitHelper.getInstance();
		jiraApiClient = retrofitHelper.getApiClient(baseUrl, username, password, JiraApiDescription.class);
	}
	
	public JiraLicense getJiraCoreLicense()
	{
		try
		{
			Call<JiraLicense> call = jiraApiClient.getJiraCoreLicense();
			Response<JiraLicense> response = call.execute();
			JiraLicense jiraLicense = response.body();
			return jiraLicense;
		}
		catch (IOException ex)
		{
			log.error("ошибка получения лицензии Jira Core", ex);
			return null;
		}
	}
	
	public JiraLicense getJiraServiceDeskLicense()
	{
		try
		{
			Call<JiraLicense> call = jiraApiClient.getJiraServiceDeskLicense();
			Response<JiraLicense> response = call.execute();
			JiraLicense jiraLicense = response.body();
			return jiraLicense;
		}
		catch (IOException ex)
		{
			log.error("ошибка получения лицензии Jira ServiceDesk", ex);
			return null;
		}
	}
	
	public JiraLicense getJiraSoftwareLicense()
	{
		try
		{
			Call<JiraLicense> call = jiraApiClient.getJiraSoftwareLicense();
			Response<JiraLicense> response = call.execute();
			JiraLicense jiraLicense = response.body();
			return jiraLicense;
		}
		catch (IOException ex)
		{
			log.error("ошибка получения лицензии Jira Software", ex);
			return null;
		}
	}
}

package pmpshk.jira.rest.api.client.entity;

import com.google.gson.annotations.SerializedName;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class JiraLicense extends BaseModel
{
	@SerializedName("organizationName")
	public String organizationName;
	
	@SerializedName("dataCenter")
	public boolean dataCenter;
	
	@SerializedName("enterprise")
	public boolean enterprise;
	
	@SerializedName("active")
	public boolean active;
	
	@SerializedName("supportEntitlementNumber")
	public String supportEntitlementNumber;
	
	@SerializedName("subscription")
	public boolean subscription;
	
	@SerializedName("rawLicense")
	public String rawLicense;
	
	@SerializedName("valid")
	public boolean valid;
	
	@SerializedName("evaluation")
	public boolean evaluation;
	
	@SerializedName("expiryDate")
	public long expiryDate;
	
	@SerializedName("licenseType")
	public String licenseType;
	
	@SerializedName("creationDateString")
	public String creationDateString;
	
	@SerializedName("expired")
	public boolean expired;
	
	@SerializedName("expiryDateString")
	public String expiryDateString;
	
	@SerializedName("autoRenewal")
	public boolean autoRenewal;
	
	@SerializedName("maximumNumberOfUsers")
	public int maximumNumberOfUsers;
}

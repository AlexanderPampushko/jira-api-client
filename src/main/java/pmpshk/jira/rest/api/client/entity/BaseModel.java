package pmpshk.jira.rest.api.client.entity;

import com.google.gson.GsonBuilder;

/**
 *
 */
public class BaseModel
{
	@Override
	public String toString()
	{
		return new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create().toJson(this);
	}
	
}
